import { useContext, useState,useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { AppContext } from "../contexts/AppContext";

export default function CategoriesAddForm({handleClose}) {

    const {setLastUpdateCategory} = useContext(AppContext)

    const [isLoading, setIsLoading] = useState(false)

    const [newCategory, setNewCategory] = useState("")

    const [type,setType] = useState("")

    useEffect(()=>{

        if(newCategory !== "" && type !== ""){
            setIsLoading(false)
        } else {
            setIsLoading(true)
        }

    },[newCategory,type])

    const handleSubmit = e =>{
        e.preventDefault()
        setIsLoading(true)
        fetch(`${process.env.REACT_APP_BE_URL}/api/categories`,{
            method: "POST",
            headers: {
                "Authorization" :`Bearer ${localStorage["token"]}`,
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                name: newCategory,
                type: type
            })
        })
        .then( res => res.json())
        .then( data => {
            setIsLoading(false)
            if (!data.error) {
                alert("Add category successful")
                handleClose()
                setLastUpdateCategory(data)
                setNewCategory("")
                setType("")
            }
        })
    }

    return (
        <Form onSubmit={handleSubmit}>
            <Form.Group controlId="name">
                <Form.Label>Name:</Form.Label>
                <Form.Control type="text"  required  value={newCategory.name} onChange={e=>setNewCategory(e.target.value)}/>
            </Form.Group>
            <Form.Group controlId="type">
                <Form.Label>Type:</Form.Label>
                <Form.Control as="select" required value={type} onChange={e=>setType(e.target.value)}>
                    <option value="" disabled>Select Type</option>
                    <option>income</option>
                    <option>expense</option>
                </Form.Control>
            </Form.Group>
            {isLoading ?
                <Button type="submit" disabled>Add Category</Button>
            :
                <Button type="submit">Add Category</Button>
            }
        </Form>
    )
}
