import React, { useContext } from 'react'
import { Navbar, Nav} from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import { AppContext } from '../contexts/AppContext'

export default function MainNavbar() {
    const { user, setUser } = useContext(AppContext)
    const history = useHistory()

    const handleClick = () =>{
        setUser({
            firstName : "",
            lastName: "",
            isAdmin: false,
            email: "",
            _id: ""
        })
        localStorage.clear()
        history.push('/')
    }
    return (
        <Navbar bg="light" expand="md">
            <Navbar.Brand as={Link} to="/">Budget Tracker</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    { user._id ? 
                        <>
                            <Nav.Link as={Link} to="/categories">Categories</Nav.Link>
                            <Nav.Link as={Link} to="/income">Income</Nav.Link>
                            <Nav.Link as={Link} to="/expenses">Expenses</Nav.Link>
                            <Nav.Link as={Link} to="/monthly">Monthly</Nav.Link>
                            <Nav.Link as={Link} to="/trend">Trend</Nav.Link>
                            <Nav.Link onClick={handleClick}>Logout</Nav.Link>
                        </> :
                        <>
                            <Nav.Link as={Link} to="/login">Login</Nav.Link>
                            <Nav.Link as={Link} to="/register">Register</Nav.Link>
                        </>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
