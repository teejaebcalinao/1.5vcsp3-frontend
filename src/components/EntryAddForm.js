import { useContext, useState,useEffect } from "react";
import { Form, InputGroup, FormControl, Button } from "react-bootstrap";
import { AppContext } from "../contexts/AppContext";

export default function EntryAddForm({type}) {
    const { categories,setLastEntryUpdate }= useContext(AppContext)

    const [isLoading, setIsLoading] = useState(false)
    const [categoriesSelect,setCategoriesSelect] = useState([])
    const [newEntry, setNewEntry] = useState({
        amount : 0,
        category: "",
        type
    })

    useEffect(()=>{

        let typeCategories = categories.filter(category => category.type === type)
        setCategoriesSelect(typeCategories)

    },[type,categories])

    const handleChange = e =>{
        setNewEntry({...newEntry, [e.target.id]: e.target.value})
    }

    const handleSubmit = e => {
        e.preventDefault()
        if(isNaN(newEntry.amount)) {
            alert("Amount should be a valid input")
        } else {

            setIsLoading(true)
            fetch(`${process.env.REACT_APP_BE_URL}/api/entries`,{
                method: "POST",
                headers: {
                    "Authorization" :`Bearer ${localStorage["token"]}`,
                    "Content-Type" : "application/json"
                },
                body: JSON.stringify(newEntry)
            })
            .then( res => res.json())
            .then( data => {
                setIsLoading(false)
                if (!data.error) {
                    alert("Add Record successful")
                    setLastEntryUpdate(data)
                }
            })
        }
    }
    return (
        <Form onSubmit={handleSubmit}>
            <Form.Group controlId="category">
                <Form.Label>Category:</Form.Label>
                <Form.Control as='select' onChange={handleChange} required>
                    <option value="" disabled selected>Choose...</option>
                    {
                        categoriesSelect.map( category => (
                            <option key={category._id} value={category.name}>{category.name}</option>
                        ))
                    }
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="amount">
                <Form.Label>Amount</Form.Label>
                <InputGroup className="mb-3">
                    <InputGroup.Text >&#8369;</InputGroup.Text>
                    <FormControl onChange={handleChange} value={newEntry.amount} required onFocus={ e=> e.target.select()}/>
                </InputGroup>
            </Form.Group>
            <Form.Group controlId="type">
                <Form.Label>Type: {type}</Form.Label>
            </Form.Group>
            {isLoading ?
                <Button type="submit" disabled>Add Record</Button>
            :
                <Button type="submit">Add Record</Button>
            }
        </Form>
    )
}
