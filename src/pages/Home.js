import { useContext } from 'react'
import { Col, Container, Row, Table, Spinner } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import LoginForm from '../components/LoginForm'
import { AppContext } from '../contexts/AppContext'

export default function Home() {

    const { user,  isLoadingEntries, entries } = useContext(AppContext)
    const totalIncome = entries.filter( elem => elem.type === "income").reduce((x,y)=> x + y.amount,0)
    const totalExpenses = entries.filter( elem => elem.type === "expense").reduce((x,y)=> x + y.amount,0)
    const totalBudget = totalIncome - totalExpenses
    return (
        <Container>
            <Row>
                <Col className="my-5 mx-auto" xs={12} sm={10} md={8} lg={6}>
                    {
                        !user._id ? 
                        <div>
                            <h1 className="text-center mb-3" >Welcome to Budget Tracking App</h1>
                            <LoginForm />
                        </div>
                    :
                        <div>
                        {
                            isLoadingEntries ?
                            <Spinner animation="border" role="status">
                                <span className="sr-only">Loading...</span>
                            </Spinner>
                            : <>
                                <h1>Summary</h1>
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th><div>Income</div> <small className="font-italic"><Link to="/income">View All</Link></small></th>
                                            <td>&#8369;<span>{totalIncome.toFixed(2)}</span></td>
                                        </tr>
                                        <tr>
                                            <th><div>Expenses</div> <small className="font-italic"><Link to="/expenses">View All</Link></small></th>
                                            <td>&#8369;<span>{totalExpenses.toFixed(2)}</span></td>
                                        </tr>
                                        <tr>
                                            <th>Balance</th>
                                            <td>&#8369;<span>{totalBudget.toFixed(2)}</span></td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </>
                        }
                        </div>
                    }
                </Col>
            </Row>
        </Container>
    )
}
