import { useState,useContext } from "react";
import { Col, Container, Row, Button, Modal } from "react-bootstrap";
import CategoriesAddForm from "../components/CategoriesAddForm";
import CategoriesList from "../components/CategoriesList";
import { AppContext } from '../contexts/AppContext'
import {Redirect} from 'react-router-dom'

export default function Categories() {
    const { user} = useContext(AppContext)
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        user._id
        ?
        <>
            <Container className="my-5">
                <Row>
                    <Col className="my-3 mx-auto">
                        <h1 className="mb-3">Categories</h1>
                        <Button variant="primary" onClick={handleShow}>
                            Add Category
                        </Button>
                                        
                    </Col>
                </Row>
                <Row>
                    <CategoriesList  />
                </Row>
            </Container>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add Category Form</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <CategoriesAddForm handleClose={handleClose} />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
        :
        <Redirect to="/"/>
    )
}
