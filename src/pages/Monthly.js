import React,{useState,useEffect,useContext} from 'react'
import { Container,Row, Col } from 'react-bootstrap'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'
import { AppContext } from "../contexts/AppContext";
import {Redirect} from 'react-router-dom'
export default function Monthly(){

	const {user,entries} = useContext(AppContext)

	const [monthExpense,setMonthExpense] = useState([])
	const [monthIncome,setMonthIncome] = useState([])

/*	console.log(entries)*/
	
	useEffect(()=>{

		if( entries.length > 0 ){
		
		let monthlyExpense = [0,0,0,0,0,0,0,0,0,0,0,0]
		        entries.forEach( entry => {
		                    const index = moment(entry.createdAt).month()//September = 8
		                    	if(entry.type === "expense" ){
		                    		monthlyExpense[index] += entry.amount
		                    	}
		                     /*console.log(monthlyExpense)  */
		        })
		   		setMonthExpense(monthlyExpense)

		let monthlyIncome = [0,0,0,0,0,0,0,0,0,0,0,0]
				entries.forEach( entry => {
				            const index = moment(entry.createdAt).month()//September = 8
				            	if(entry.type === "income" ){
				            		monthlyIncome[index] += entry.amount
				            	}
				             /*console.log(monthlyIncome) */ 
				})

				setMonthIncome(monthlyIncome)
		}

	},[entries])

/*	console.log(monthExpense)
	console.log(monthIncome)
*/
	const dataIncome = {

		        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November',  'December'],
		        datasets: [

		            {   
		                label: "Monthly Income",
		                backgroundColor: 'skyblue',
		                borderColor: 'black',
		                borderWidth: 1,
		                hoverBackgroundColor: 'blue',
		                hoverBorderColor: 'black',
		                data: monthIncome

		            }
		        ]

		    }

	const dataExpense = {

		        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November',  'December'],
		        datasets: [

		            {   
		                label: "Monthly Expense",
		                backgroundColor: 'indianRed',
		                borderColor: 'black',
		                borderWidth: 1,
		                hoverBackgroundColor: 'red',
		                hoverBorderColor: 'black',
		                data: monthExpense

		            }
		        ]

		    }

	return (
		user._id
		?
		<Container>
			<Row id="charts">
				<Col xs={12} md={6}>
					<h3 className="text-center mt-5">Monthly Income in PHP</h3>
					<Bar data={dataIncome} />		
				</Col>
				<Col xs={12} md={6}>
					<h3 className="text-center mt-5">Monthly Expense in PHP</h3>
					<Bar data={dataExpense} />		
				</Col>
			</Row>
		</Container>
		:
		<Redirect to="/" />

		)
}