import { Col, Container, Row } from 'react-bootstrap'
import LoginForm from '../components/LoginForm'
import { useContext } from 'react'
import { AppContext } from '../contexts/AppContext'
import {Redirect} from 'react-router-dom'
export default function Login() {
    const { user} = useContext(AppContext)
    return (
        user._id
        ?
        <Redirect to="/"/>
        :
        <Container>
            <Row>
                <Col xs={12} sm={10} md={8} lg={6} className="mx-auto my-5">
                    <h1 className="text-center mb-3">Login Form</h1>
                    <LoginForm />
                </Col>
            </Row>
        </Container>
    )
}
