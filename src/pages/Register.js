import { useContext } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import RegisterForm from '../components/RegisterForm'
import { AppContext } from '../contexts/AppContext'
import {Redirect} from 'react-router-dom'
export default function Register() {
    const { user} = useContext(AppContext)
    return (
        user._id
        ?
        <Redirect to="/"/>
        :
        <Container>
            <Row>
                <Col xs={12} sm={10} md={8} lg={6} className="mx-auto my-5">
                    <h1 className="text-center mb-3">Register</h1>
                    <RegisterForm />
                </Col>
            </Row>
        </Container>
    )
}
