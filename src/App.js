import { BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import './App.css';
import MainNavbar from './components/MainNavbar';
import AppContextProvider from './contexts/AppContext';
import Categories from './pages/Categories';
import Error404 from './pages/Error404';
import Expenses from './pages/Expenses';
import Monthly from './pages/Monthly';
import Trend from './pages/Trend';
import Home from './pages/Home';
import Income from './pages/Income';
import Login from './pages/Login';
import Register from './pages/Register';

function App() {
  return (
    <div className="App">
      <AppContextProvider>
        
        <Router>
          <MainNavbar />

          <Switch>
            <Route path="/" exact ><Home /></Route>
            <Route path="/income" exact ><Income /></Route>
            <Route path="/expenses" exact ><Expenses /></Route>
            <Route path="/login" exact ><Login /></Route>
            <Route path="/register" exact ><Register /></Route>
            <Route path="/categories" exact ><Categories /></Route>
            <Route path="/monthly" exact ><Monthly /></Route>
            <Route path="/trend" exact ><Trend /></Route>
            <Route path="*"><Error404 /></Route>
          </Switch>
        </Router>
      </AppContextProvider>
    </div>
  );
}

export default App;
