import { createContext, useEffect, useState } from "react";

export const AppContext = createContext()

export default function AppContextProvider(props) {

    const [user, setUser] = useState({
        firstName : "",
        lastName: "",
        isAdmin: false,
        email: "",
        _id: ""
    })
    const [isLoadingUser, setIsLoadingUser] = useState(false)

    const [categories, setCategories] = useState([])
    const [lastUpdateCategory, setLastUpdateCategory] = useState({})
    const [isLoadingCategory, setisLoadingCategory] = useState(false)

    const [lastEntryUpdate, setLastEntryUpdate] = useState({})
    const [entries, setEntries] = useState([])
    const [isLoadingEntries, setisLoadingEntries] = useState(false)

    const [totalIncome, setTotalIncome] = useState(0)
    const [totalExpenses, setTotalExpenses] = useState(0)
    const [totalBudget, setTotalBudget] = useState(0)
    
    useEffect(() => {
        if (localStorage["token"]){
            setIsLoadingUser(true)
            fetch(`${process.env.REACT_APP_BE_URL}/api/users`,{
                headers: {
                    "Authorization" : `Bearer ${localStorage["token"]}`
                }
            })
            .then( res => res.json())
            .then( data => {
                setIsLoadingUser(false)
                if(data.auth !== "failed") {
                    setUser(data)
                }
            })
        }

    }, [])



    useEffect(()=>{
        setisLoadingCategory(true)
        fetch(`${process.env.REACT_APP_BE_URL}/api/categories`,{
            headers: {
                'Authorization' : `Bearer ${localStorage["token"]}`
            }
        })
        .then( res => res.json())
        .then( data => {
            setisLoadingCategory(false)
            if(data.auth !== "failed"){
                setCategories(data)
            }
        })
    },[lastUpdateCategory,lastEntryUpdate,user])


    useEffect(()=>{
        setisLoadingEntries(true)
        fetch(`${process.env.REACT_APP_BE_URL}/api/entries`,{
            headers: {
                'Authorization' : `Bearer ${localStorage["token"]}`
            }
        })
        .then( res => res.json())
        .then( data => {
            setisLoadingEntries(false)
            if(data.auth !== "failed"){
                setEntries(data)
                setTotalIncome(entries.filter( entry => entry.type === "income").reduce((x,y) => x + y.amount, 0))
                setTotalExpenses(entries.filter( entry => entry.type === "expense").reduce((x,y) => x + y.amount, 0))
                setTotalBudget(totalIncome - totalExpenses)
            }
        })
    },[lastEntryUpdate,lastUpdateCategory,user])


    return (
        <>
        {isLoadingUser ? 
            "Loading ..."
        :
        <AppContext.Provider value={{
            user, 
            setUser, 
            categories, 
            setCategories,
            setLastUpdateCategory,
            entries,
            setLastEntryUpdate,
            isLoadingCategory,
            isLoadingEntries,
            totalIncome,
            totalExpenses,
            totalBudget
        }}>
            {props.children}
        </AppContext.Provider>
        }
        </>
    )
}